The Community branch of the MMMerge project - Might and Magic Merge, also known as the World of Enroth.
Check the thread here: https://www.celestialheavens.com/forum/10/16657

# WILL ONLY WORK WITH MERGE VERSION 12.07.2020!

### Installation instructions

https://gitlab.com/templayer/mmmerge/-/wikis/Docs/PlayerManual/Install

0. Install the Merge - by installing the original game, then the merge up to the compatible version mentioned (if the newest one by Rodril is newer, please send me an email to templayer@seznam.cz and I will update the community branch) and the newest GrayFace patch on top of that (link is in the first post in the thread)
1. Download either the branch with community fixes only, or the full Community Branch (that has community features). 
* Only Fixes: https://gitlab.com/templayer/mmmerge/-/archive/rodril-fixes/mmmerge-rodril-fixes.zip 
* Fully Featured: https://gitlab.com/templayer/mmmerge/-/archive/master/mmmerge-master.zip
2. Delete the script folder in your installation folder (we had problems with rogue lua files before...)
3. Copy paste the folders from GitLab into your installation directory, replacing existing (except for the script folder mentioned in the previous point... that one will be added instead of replaced).
4. Use and configure any optional content in the "Changes from the Vanilla (Base / non-community branch) Merge" (i.e. following) section
5. Play.

### Changes from the Vanilla (Base / non-community branch) Merge

(See also https://gitlab.com/templayer/mmmerge/-/wikis/Features)

1. Changed class system.
2. Changed race system.
3. Custom Race/Class names support (class/race combination can have special visible name).
4. Different Base and Bonus skill value limits: base skill value can be raised up to 80 by default. (Also different mm8 code patching.)
5. Configurable race resistances.
6. Implemented Stealing
7. Bolster Monster modifications - Fanasilver made a modification for a less aggressive HP scaling called "Use a non-aggresive formula for HP calculation". Results:

| Bolster | HP coefficient (× Base Health) |
|---------|--------------------------------|
| 0%      | 1                              |
| 100%    | 2                              |
| 150%    | 4                              |
| 200%    | 9                              |

Bolster can be put to original behavior by replacing `Data/Tables/Bolster - formulas.txt` with `Extra/BaseDefault/Bolster - formulas.txt`.  
Bolster can be put back to Fanasilver's by replacing `Data/Tables/Bolster - formulas.txt` with `Extra/CommunityDefault/Bolster - formulas.txt`.

8. Icon - there is an icon contained within the branch, made by Templayer out of the Dracolich assets, which were made by GDSpectra, Jamesx and Templayer. Should be compatible with Windows XP, but still includes a 256x256 image variant for systems that support such high-res icons. (also GitLab is telling me that it only contains the 256x256, which is false...)
9. Debug "tent" button at character creation to tell you the indexes of your choices (currently non-toggleable) by majaczek
4. Zombies will become Undead when made through Character Creation in order not to have the Zombie status (toggleable) by majaczek
5. Racial Skills are automatically learned if a character was created by using the Character Creation screen (toggleable) by majaczek
6. Better Autobiographies for Characters Created During Character Creation by adding Race (toggleable) by majaczek
7. Druid Summoning ALPHA - By holding CTRL while casting a targetting Earth spell by a Druid, summons creatures instead. I won't give you a list of creatures yet, since those are probably going change. By wuxiangjinxing
8. UI Crash Hotfix by CkNoSFeRaTU
9. Race Kinds and extended Race Names support, Game.Races structure and Races.txt table by cthscr (Issue #1)
10. User settings in `MMMergeSettings.lua` file - see [PlayerManual/Configuration](https://gitlab.com/templayer/mmmerge/-/wikis/Docs/PlayerManual/Configuration) for details.
11. Check the Extra folder for different sets of table settings. Currently we have BaseDefault (close to Rodril's) and CommunityDefault.
12. Check the Extra/1FileMod folder for small one-file optional table settings (like the Character Creation Unlocker or Racial Skills Alternate table). To use them, replace the existing in Data/Tables folder with whatever you choose.

### Changelog

https://gitlab.com/templayer/mmmerge/-/wikis/Changelog

### TODOs
1. Make the custom optional modifications toggleable.
List of things to make toggleable - Merge Request 2 (debug "Tent" button), Merge Request 9 - Druid Summoning 
2. Implement stuff on the Implementation Queue on the Merge Tracker - https://goo.gl/ui24Bz
3. Finish Up Druid Summoning - Summoner.lua
4. Replace UI Crash Hotfix with a proper solution (probably one made by Rodril or GrayFace) when it comes out

### Developer notes
1. For some basic code readability instructions (when writing your own code for the Merge), check the Snippets. There is currently only one, for Magical Numbers.

### Community Branch Credits
Maintenance: Templayer (semi-retired)  
Coding: majaczek, fanasilver, CkNoSFeRaTU, wuxiangjinxing, cthscr, GrayFace, Rodril

