#	StringId	Mature	BaseRace	Family	Kind	Name	Plural	Adjective
0	Human	0	0	0	0	Human	Humans	Human
1	MatureHuman	1	0	0	0	Mature Human	Mature Humans	Mature Human
2	UndeadHuman	0	0	1	1	Undead Human	Undead Humans	Undead Human
3	MatureUndeadHuman	1	0	1	1	Mature Undead Human	Mature Undead Humans	Mature Undead Human
4	VampireHuman	0	0	2	1	Vampire Human	Vampire Humans	Vampire Human
5	MatureVampireHuman	1	0	2	1	Mature Vampire Human	Mature Vampire Humans	Mature Vampire Human
6	ZombieHuman	0	0	3	1	Zombie Human	Zombie Humans	Zombie Human
7	ZombieUndeadHuman	1	0	3	1	Mature Zombie Human	Mature Zombie Humans	Mature Zombie Human
8	GhostHuman	0	0	4	1	Ghost Human	Ghost Humans	Ghost Human
9	MatureGhostHuman	1	0	4	1	Mature Ghost Human	Mature Ghost Humans	Mature Ghost Human
10	DarkElf	0	10	0	2	Dark Elf	Dark Elves	Dark Elven
11	MatureDarkElf	1	10	0	2	Mature Dark Elf	Mature Dark Elves	Mature Dark Elven
12	UndeadDarkElf	0	10	1	1	Undead Dark Elf	Undead Dark Elves	Undead Dark Elven
13	MatureUndeadDarkElf	1	10	1	1	Mature Undead Dark Elf	Mature Undead Dark Elves	Mature Undead Dark Elven
14	VampireDarkElf	0	10	2	1	Vampire Dark Elf	Vampire Dark Elves	Vampire Dark Elven
15	MatureVampireDarkElf	1	10	2	1	Mature Vampire Dark Elf	Mature Vampire Dark Elves	Mature Vampire Dark Elven
16	ZombieDarkElf	0	10	3	1	Zombie Dark Elf	Zombie Dark Elves	Zombie Dark Elven
17	MatureZombieDarkElf	1	10	3	1	Mature Zombie Dark Elf	Mature Zombie Dark Elves	Mature Zombie Dark Elven
18	GhostDarkElf	0	10	4	1	Ghost Dark Elf	Ghost Dark Elves	Ghost Dark Elven
19	MatureGhostDarkElf	1	10	4	1	Mature Ghost Dark Elf	Mature Ghost Dark Elves	Mature Ghost Dark Elven
20	Minotaur	0	20	0	3	Minotaur	Minotaurs	Minotaur
21	MatureMinotaur	1	20	0	3	Mature Minotaur	Mature Minotaurs	Mature Minotaur
22	UndeadMinotaur	0	20	1	1	Undead Minotaur	Undead Minotaurs	Undead Minotaur
23	MatureUndeadMinotaur	1	20	1	1	Mature Undead Minotaur	Mature Undead Minotaurs	Mature Undead Minotaur
24	VampireMinotaur	0	20	2	1	Vampire Minotaur	Vampire Minotaurs	Vampire Minotaur
25	MatureVampireMinotaur	1	20	2	1	Mature Vampire Minotaur	Mature Vampire Minotaurs	Mature Vampire Minotaur
26	ZombieMinotaur	0	20	3	1	Zombie Minotaur	Zombie Minotaurs	Zombie Minotaur
27	MatureZombieMinotaur	1	20	3	1	Mature Zombie Minotaur	Mature Zombie Minotaurs	Mature Zombie Minotaur
28	GhostMinotaur	0	20	4	1	Ghost Minotaur	Ghost Minotaurs	Ghost Minotaur
29	MatureGhostMinotaur	1	20	4	1	Mature Ghost Minotaur	Mature Ghost Minotaurs	Mature Ghost Minotaur
30	Troll	0	30	0	4	Troll	Trolls	Troll
31	MatureTroll	1	30	0	4	Mature Troll	Mature Trolls	Mature Troll
32	UndeadTroll	0	30	1	1	Undead Troll	Undead Trolls	Undead Troll
33	MatureUndeadTroll	1	30	1	1	Mature Undead Troll	Mature Undead Trolls	Mature Undead Troll
34	VampireTroll	0	30	2	1	Vampire Troll	Vampire Trolls	Vampire Troll
35	MatureVampireTroll	1	30	2	1	Mature Vampire Troll	Mature Vampire Trolls	Mature Vampire Troll
36	ZombieTroll	0	30	3	1	Zombie Troll	Zombie Trolls	Zombie Troll
37	MatureZombieTroll	1	30	3	1	Mature Zombie Troll	Mature Zombie Trolls	Mature Zombie Troll
38	GhostTroll	0	30	4	1	Ghost Troll	Ghost Trolls	Ghost Troll
39	MatureGhostTroll	1	30	4	1	Mature Ghost Troll	Mature Ghost Trolls	Mature Ghost Troll
40	Dragon	0	40	0	5	Dragon	Dragons	Dragon
41	MatureDragon	1	40	0	5	Mature Dragon	Mature Dragons	Mature Dragon
42	UndeadDragon	0	40	1	1	Undead Dragon	Undead Dragons	Undead Dragon
43	MatureUndeadDragon	1	40	1	1	Mature Undead Dragon	Mature Undead Dragons	Mature Undead Dragon
44	VampireDragon	0	40	2	1	Vampire Dragon	Vampire Dragons	Vampire Dragon
45	MatureVampireDragon	1	40	2	1	Mature Vampire Dragon	Mature Vampire Dragons	Mature Vampire Dragon
46	ZombieDragon	0	40	3	1	Zombie Dragon	Zombie Dragons	Zombie Dragon
47	MatureZombieDragon	1	40	3	1	Mature Zombie Dragon	Mature Zombie Dragons	Mature Zombie Dragon
48	GhostDragon	0	40	4	1	Ghost Dragon	Ghost Dragons	Ghost Dragon
49	MatureGhostDragon	1	40	4	1	Mature Ghost Dragon	Mature Ghost Dragons	Mature Ghost Dragon
50	Elf	0	50	0	2	Elf	Elves	Elven
51	MatureElf	1	50	0	2	Mature Elf	Mature Elves	Mature Elven
52	UndeadElf	0	50	1	1	Undead Elf	Undead Elves	Undead Elven
53	MatureUndeadElf	1	50	1	1	Mature Undead Elf	Mature Undead Elves	Mature Undead Elven
54	VampireElf	0	50	2	1	Vampire Elf	Vampire Elves	Vampire Elven
55	MatureVampireElf	1	50	2	1	Mature Vampire Elf	Mature Vampire Elves	Mature Vampire Elven
56	ZombieElf	0	50	3	1	Zombie Elf	Zombie Elves	Zombie Elven
57	MatureZombieElf	1	50	3	1	Mature Zombie Elf	Mature Zombie Elves	Mature Zombie Elven
58	GhostElf	0	50	4	1	Ghost Elf	Ghost Elves	Ghost Elven
59	MatureGhostElf	1	50	4	1	Mature Ghost Elf	Mature Ghost Elves	Mature Ghost Elven
60	Goblin	0	60	0	6	Goblin	Goblins	Goblin
61	MatureGoblin	1	60	0	6	Mature Goblin	Mature Goblins	Mature Goblin
62	UndeadGoblin	0	60	1	1	Undead Goblin	Undead Goblins	Undead Goblin
63	MatureUndeadGoblin	1	60	1	1	Mature Undead Goblin	Mature Undead Goblins	Mature Undead Goblin
64	VampireGoblin	0	60	2	1	Vampire Goblin	Vampire Goblins	Vampire Goblin
65	MatureVampireGoblin	1	60	2	1	Mature Vampire Goblin	Mature Vampire Goblins	Mature Vampire Goblin
66	ZombieGoblin	0	60	3	1	Zombie Goblin	Zombie Goblins	Zombie Goblin
67	MatureZombieGoblin	1	60	3	1	Mature Zombie Goblin	Mature Zombie Goblins	Mature Zombie Goblin
68	GhostGoblin	0	60	4	1	Ghost Goblin	Ghost Goblins	Ghost Goblin
69	MatureGhostGoblin	1	60	4	1	Mature Ghost Goblin	Mature Ghost Goblins	Mature Ghost Goblin
70	Dwarf	0	70	0	7	Dwarf	Dwarves	Dwarven
71	MatureDwarf	1	70	0	7	Mature Dwarf	Mature Dwarves	Mature Dwarven
72	UndeadDwarf	0	70	1	1	Undead Dwarf	Undead Dwarves	Undead Dwarven
73	MatureUndeadDwarf	1	70	1	1	Mature Undead Dwarf	Mature Undead Dwarves	Mature Undead Dwarven
74	VampireDwarf	0	70	2	1	Vampire Dwarf	Vampire Dwarves	Vampire Dwarven
75	MatureVampireDwarf	1	70	2	1	Mature Vampire Dwarf	Mature Vampire Dwarves	Mature Vampire Dwarven
76	ZombieDwarf	0	70	3	1	Zombie Dwarf	Zombie Dwarves	Zombie Dwarven
77	MatureZombieDwarf	1	70	3	1	Mature Zombie Dwarf	Mature Zombie Dwarves	Mature Zombie Dwarven
78	GhostDwarf	0	70	4	1	Ghost Dwarf	Ghost Dwarves	Ghost Dwarven
79	MatureGhostDwarf	1	70	4	1	Mature Ghost Dwarf	Mature Ghost Dwarves	Mature Ghost Dwarven
