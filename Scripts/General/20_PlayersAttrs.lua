-- Save and restore Player.Attrs to/from vars.PlayersAttrs

function events.GameInitialized2()
	Game.PlayersAttrs = Game.PlayersAttrs or {}

	-- Rod:
	-- "Player" is lua table with custom metatable, thus we have
	--     to edit metatable to properly add new field:
	local player = Party.PlayersArray[0]
	local metatable = getmetatable(player)
	if not metatable.offsets.Attrs then
		metatable.offsets.Attrs = 0
		metatable.members.Attrs = function(offset, parent, field, value)
			local playerId = (parent["?ptr"] - Party.PlayersArray["?ptr"])/parent["?size"]
			if value then
				Game.PlayersAttrs[playerId] = value
			else
				return Game.PlayersAttrs[playerId]
			end
		end
		setmetatable(player, metatable)
	end
	-- Rod.
end

function events.BeforeSaveGame()
	Log(Merge.Log.Info, "PlayersAttrs: BeforeSaveGame")
	vars.PlayersAttrs = Game.PlayersAttrs
end

function events.BeforeLoadMap(WasInGame)
	if not WasInGame then
		Log(Merge.Log.Info, "PlayersAttrs: BeforeLoadMap")
		Game.PlayersAttrs = vars.PlayersAttrs or Game.PlayersAttrs
		if Game.PlayersAttrs == nil then
			Log(Merge.Log.Info, "PlayersAttrs: neither Game.PlayersAttrs nor vars.Players.Attrs")
			Game.PlayersAttrs = {}
		end

		for k = 0, Party.PlayersArray.count - 1 do
			local player = Party.PlayersArray[k]
			--Log(Merge.Log.Info, "PlayersAttrs: Player %d", k)
			Game.PlayersAttrs[k] = Game.PlayersAttrs[k] or {}
			if player.Attrs.Race == nil then
				player.Attrs.Race = GetCharRace(player)
				Log(Merge.Log.Info, "Set default race %d for player %d", player.Attrs.Race, k)
			end
			if player.Attrs.PromoAwards == nil then
				player.Attrs.PromoAwards = {}
			end
		end
	end
end
