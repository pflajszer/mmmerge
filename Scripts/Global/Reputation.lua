local LogId = "Reputation"
local Log = Log
Log(Merge.Log.Info, "Init started: %s", LogId)

local ceil, floor, max = math.ceil, math.floor, math.max
local u4 = mem.u4

vars.GlobalReputation	= vars.GlobalReputation or {}
vars.ContinentFame		= vars.ContinentFame or {} -- contains exp base for fame counting.

local ShopsClosed = false
local ContSet

local function ExitBTBTopics(npc)
	NPCFollowers.ClearEvents(Game.NPC[npc])

	NPCFollowers.SetHireTopic(npc)
	NPCFollowers.SetNewsTopics(npc)
end

-- Beg topic.
Game.GlobalEvtLines:RemoveEvent(NPCFollowers.BegTopic)
evt.Global[NPCFollowers.BegTopic] = function()
	if Game.CurrentPlayer < 0 then
		return
	end

	local MerchantSkill = SplitSkill(Party[Game.CurrentPlayer]:GetSkill(const.Skills.Merchant))
	local npc = GetCurrentNPC()
	local PersSet = Game.NPCPersonalities[Game.NPCProf[Game.NPC[npc].Profession].Personality]
	local NPCExtra = mapvars.MapNPCNews[npc]

	if NPCExtra.BegSuccess == Game.DayOfMonth then
		Message(NPCFollowers.PrepareBTBString(npc, PersSet.BegRet))
		Party[Game.CurrentPlayer]:ShowFaceAnimation(const.FaceAnimation.BegFail)
	elseif PersSet.AcceptBeg then
		NPCExtra.BegSuccess = Game.DayOfMonth
		Message(NPCFollowers.PrepareBTBString(npc, PersSet.BegSuccess))
		ExitBTBTopics(npc)
		Party[Game.CurrentPlayer]:ShowFaceAnimation(const.FaceAnimation.Beg)
	else
		Message(NPCFollowers.PrepareBTBString(npc, PersSet.BegFail))
		Party[Game.CurrentPlayer]:ShowFaceAnimation(const.FaceAnimation.BegFail)
	end
end

-- Threat topic
Game.GlobalEvtLines:RemoveEvent(NPCFollowers.ThreatTopic)
evt.Global[NPCFollowers.ThreatTopic] = function()
	if Game.CurrentPlayer < 0 then
		return
	end

	local npc = GetCurrentNPC()
	local PersSet = Game.NPCPersonalities[Game.NPCProf[Game.NPC[npc].Profession].Personality]

	if PersSet.AcceptThreat then
		mapvars.MapNPCNews[npc].ThreatSuccess = Game.DayOfMonth
		Message(NPCFollowers.PrepareBTBString(npc, PersSet.ThreatSuccess))
		ExitBTBTopics(npc)
	else
		Message(NPCFollowers.PrepareBTBString(npc, PersSet.ThreatFail))
		Party[Game.CurrentPlayer]:ShowFaceAnimation(const.FaceAnimation.ThreatFail)
	end
end

-- Bribe topic.
Game.GlobalEvtLines:RemoveEvent(NPCFollowers.BribeTopic)
evt.Global[NPCFollowers.BribeTopic] = function()
	local npc = GetCurrentNPC()
	local ProfSet = Game.NPCProf[Game.NPC[npc].Profession]
	local Cost = ProfSet.Cost or 50
	local PersSet = Game.NPCPersonalities[ProfSet.Personality]

	evt.ForPlayer(0)

	if PersSet.AcceptBribe then
		if Party.Gold > Cost then
			evt.Subtract("Gold", Cost)
			mapvars.MapNPCNews[npc].BribeSuccess = Game.DayOfMonth
			Message(NPCFollowers.PrepareBTBString(npc, PersSet.BribeSuccess))
			ExitBTBTopics(npc)
		else
			Message(Game.GlobalTxt[155])
			Party[Game.CurrentPlayer]:ShowFaceAnimation(const.FaceAnimation.BribeFail)
		end
	else
		Message(NPCFollowers.PrepareBTBString(npc, PersSet.BribeFail))
		Party[Game.CurrentPlayer]:ShowFaceAnimation(const.FaceAnimation.BribeFail)
	end
end


local StdBribeTopic = Game.NPCTopic[1765]
function events.EnterNPC(i)
	local Cost = Game.NPCProf[Game.NPC[i].Profession].Cost or 50
	Game.NPCTopic[NPCFollowers.BribeTopic] = StdBribeTopic .. " " .. tostring(Cost) .. " " .. Game.GlobalTxt[97]
end

----

--[[
local function ChangeShopsState(State)
	State = State or 0

	local T = Game.HousesByMaps[Map.MapStatsIndex]
	local BanTime = State == 0 and 0 or Game.Time + const.Day
	if T then
		for k,v in pairs(T) do
			if k >= 1 and k <= 4 then
				for _,i in pairs(v) do
					Game.ShopBanExpiration[GetHouseWritePos(i)] = BanTime
				end
			end
		end
	end
end
]]

local function ChangeGuardsState(State)
	for i,v in Map.Monsters do
		if v.Group == 38 or v.Group == 55 then -- default groups for map guards.
			v.Hostile = State
		end
	end
end

local function GetPartyReputation()
	return mem.call(0x47603F) --mem.i4[0x6cf0a4]
end
NPCFollowers.GetPartyReputation = GetPartyReputation

local function GetFameBase()
	local CurCont = TownPortalControls.MapOfContinent(Map.MapStatsIndex)
	if CurCont == 4 then
		return Party[0].Experience
	else
		local Total = 0

		for k,v in pairs(vars.ContinentFame) do
			Total = Total + v
		end

		local res = Party[0].Experience - Total + (vars.ContinentFame[CurCont] or 0)
		vars.ContinentFame[CurCont] = res

		return res
	end
end

local function GetPartyFame()
	return Party.GetFame()
end
NPCFollowers.GetPartyFame = GetPartyFame

local function StoreReputation()
	local CurCont = TownPortalControls.MapOfContinent(Map.MapStatsIndex)

	vars.GlobalReputation[CurCont] = GetPartyReputation()
end


local BanText = Game.NPCText[1683]

local function ExitShopAfter(t)
	if t.Key == 27 then -- escape
		ExitCurrentScreen()
		events.Remove("KeysFilter", ExitShopAfter)
	end
end

local function ShowBanText()
	events.KeysFilter = ExitShopAfter
	Game.EscMessage(BanText)
end

----

function events.GetFameBase(t)
	t.Base = GetFameBase() or 0
end

local PEASANT = const.Bolster.Creed.Peasant
function events.MonsterKilled(mon, monIndex, defaultHandler, killer)

	if not (ContSet.UseRep or ContSet.RepPenKillPeas or ContSet.RepPenKillGuard) then
		return
	end

	-- affect reputation only if monster killed by party, and monster was not reanimated.
	if killer.Type == 4 and mon.Ally ~= 9999 then

		local CurCont = TownPortalControls.MapOfContinent(Map.MapStatsIndex)
		local MonExtra = Game.Bolster.MonstersSource[mon.Id]
		local ContRep = vars.GlobalReputation[CurCont]

		evt.ForPlayer(0)

		local rep_changed = false
		-- Subtract Reputation if peasant killed (peasants can not be bounty hunt targets or arena participants).
		if MonExtra.Creed == PEASANT and ContSet.RepPenKillPeas then
			evt.Add("Reputation", ContSet.RepPenKillPeas)
			rep_changed = true
		end
		if (mon.Group == 38 or mon.Group == 55) and ContSet.RepPenKillGuard then
			evt.Add("Reputation", ContSet.RepPenKillPeas)
			rep_changed = true
		end
		if rep_changed then
			--if not evt.Cmp{"Reputation", 100} then -- Don't drop reputation below 100.
			--	evt.Add{"Reputation", 1} -- Add and Sub kind of reverted for reputation.

			--	if ContRep < 20 then
			--		vars.GlobalReputation[CurCont] = ContRep + 1
			--		evt.Add{"Reputation", 1} -- Let party get murderer reputation across continent
			--	end
			--end

			local PartyRep = GetPartyReputation()

			if ContSet.RepGuards and PartyRep >= ContSet.RepGuards then
				ChangeGuardsState(true)
			end

			return
		end

		if not ContSet.UseRep then return end
		-- Increase Reputation if monster is bounty hunt target.
		local BH = vars.BountyHunt and vars.BountyHunt[Map.Name]
		if BH and not BH.Done and BH.MonId == mon.Id then
			local Reward = math.floor(mon.Level/20)

			if evt.Cmp{"Reputation", -20} then
				evt.Subtract{"Reputation", Reward}
			end

			-- Let party adjust global reputation by BH quests.
			if ContRep > -5 then
				vars.GlobalReputation[CurCont] = ContRep - 1
			end

			return
		end

	end

end

function events.ClickShopTopic(t)

	if not ContSet.UseRep then
		return
	end

	local Rep = GetPartyReputation()
	if Rep > 0 then
		local cHouse = Game.Houses[GetCurrentHouse()]

		if t.Topic == const.ShopTopics.Donate and cHouse.C == 0 then

		--	local Amount = Game.Houses[GetCurrentHouse()].Val
		--	local cGold = Party.Gold
		--	if Party.Gold >= Amount then
		--		if Rep > 0 then
		--			Party.Gold = cGold - math.min((math.floor(Amount*Rep/5)), cGold) + Amount
		--		end

		--		if ContSet.RepGuards and Rep < ContSet.RepGuards then
		--			ChangeGuardsState(false)
		--		end
		--	end

		elseif table.find({const.HouseType.Boats, const.HouseType.Stables, const.HouseType.Temple}, cHouse.Type) then
			return

		elseif ContSet.RepShops and Rep > ContSet.RepShops then
			t.Handled = true
			ShowBanText()

		end
	end
end

function events.BeforeSaveGame()
	StoreReputation()
end

function events.LeaveMap()
	StoreReputation()
end

function events.AfterLoadMap()

	local CurCont = TownPortalControls.MapOfContinent(Map.MapStatsIndex) or TownPortalControls.GetCurrentSwitch()

	ContSet = Game.ContinentSettings[CurCont]

	if not ContSet.UseRep then
		return
	end

	vars.GlobalReputation[CurCont] = vars.GlobalReputation[CurCont] or 0

	local State

	-- Separate reputation by continents
	local CurRep = vars.GlobalReputation[CurCont]

	evt.Set{"Reputation", CurRep}

	-- Make guards aggressive
	if ContSet.RepGuards then
		State = CurRep >= ContSet.RepGuards
		ChangeGuardsState(State)
	end

end

local ht = const.HouseType
-- Close shops for party with bad reputation
function events.OnEnterShop(t)
	if ContSet.RepShops then
		local house = Game.Houses[GetCurrentHouse()]
		if table.find({ht.Boats, ht.Stables, ht.Temple}, house.Type) then
			return
		end
		t.Banned = GetPartyReputation() >= ContSet.RepShops
	end
end

function events.ReputationTradeEffect(t)
	if not ContSet.UseRep then
		return
	end
	if ContSet.RepTradeMult then
		t.RepTradeEffect = ceil(t.RepTradeEffect * ContSet.RepTradeMult)
	end
end

function events.ReputationChanged(t)
	if ContSet.RepGuards then
		State = t.CurrentValue >= ContSet.RepGuards
		ChangeGuardsState(State)
	end
end

function events.DonationCost(t)
	if not ContSet.UseRep then
		return
	end
	local cur_rep = GetPartyReputation()
	if cur_rep <= 0 then
		return
	end
	local house = Game.Houses[GetCurrentHouse()]
	local amount = house.Val
	-- Default multiplier was 0.2
	if ContSet.RepDonateCMult then
		t.Cost = max(amount, floor(amount * cur_rep * ContSet.RepDonateCMult))
	end
end

function events.DonationReputationChange(t)
	if ContSet.RepDonateMod then
		t.Subtractor = ContSet.RepDonateMod
	end
	if ContSet.RepDonateLim then
		t.Limit = ContSet.RepDonateLim
	end
end

function events.DonationSpell(t)
	if not ContSet.RepDonateSpells then
		return
	end
	if ContSet.RepDonateSpells == 2 then
		if t.Reputation < -24 then
			t.Spell = 85
		elseif t.Reputation < -19 then
			t.Spell = 86
		elseif t.Reputation < -14 then
			t.Spell = 75
		elseif t.Reputation < -9 then
			t.Spell = 50
		else
			t.Cast = false
		end
	elseif ContSet.RepDonateSpells == 3 then
		if t.Reputation < -999 then
			t.Spell = 85
		elseif t.Reputation < -799 then
			t.Spell = 86
		elseif t.Reputation < -599 then
			t.Spell = 83
		else
			t.Cast = false
		end
	end
end

function events.GetReputationRankStr(t)
	if not ContSet.RepRanks then
		return
	end
	if ContSet.RepRanks == 3 then
		if t.Reputation < -999 then
			t.Ptr = u4[0x601448 + 510 * 4]
		elseif t.Reputation < -799 then
			t.Ptr = u4[0x601448 + 511 * 4]
		elseif t.Reputation < -599 then
			t.Ptr = u4[0x601448 + 512 * 4]
		elseif t.Reputation < -399 then
			t.Ptr = u4[0x601448 + 513 * 4]
		elseif t.Reputation < -199 then
			t.Ptr = u4[0x601448 + 514 * 4]
		elseif t.Reputation > 999 then
			t.Ptr = u4[0x601448 + 520 * 4]
		elseif t.Reputation > 799 then
			t.Ptr = u4[0x601448 + 519 * 4]
		elseif t.Reputation > 599 then
			t.Ptr = u4[0x601448 + 518 * 4]
		elseif t.Reputation > 399 then
			t.Ptr = u4[0x601448 + 517 * 4]
		elseif t.Reputation > 199 then
			t.Ptr = u4[0x601448 + 516 * 4]
		else
			t.Ptr = u4[0x601448 + 515 * 4]
		end
	end
end

Log(Merge.Log.Info, "Init finished: %s", LogId)

