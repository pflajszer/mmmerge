-- Dagger Wound Island

-- Dimension door
function events.TileSound(t)
	if t.X == 63 and t.Y == 59 then
		TownPortalControls.DimDoorEvent()
	end
end

function events.AfterLoadMap()
	Party.QBits[801] = true	-- DDMapBuff
end
