local LogId = "RemoveSkillBonusLimits"
local Log = Log
Log(Merge.Log.Info, "Init started: %s", LogId)

-- MaxSkillVal is maximum value of base_skill + skill_bonus
-- Should be 2^N-1 with maximum value of 0x3FF
-- MM8 value is 0x3F
local MaxSkillVal = 0xFF
-- MaxSkillBase is maximum value of base_skill
-- MM8 value is 0x3C
local MaxSkillBase = 0x50
-- Bit offset of Expert Mastery
-- Shouldn't be less than log2(MaxSkillVal + 1) [N from above]
-- MM8 value is 6
local ExpertBit = 10

-- WARNING: there are a lot of hardcoded values in this file still.

local floor, max, pow = math.floor, math.max, math.pow
local asmpatch = mem.asmpatch
local strformat = string.format

local MaxSkillValH = floor(MaxSkillVal / 0x100)
local SkillAnd = 0xFFFFFFFF - MaxSkillVal
local SkillAndX = 0xFFFF - MaxSkillVal
local MaxSkillValStr, MaxSkillValHStr = strformat("0x%X", MaxSkillVal), strformat("0x%X", MaxSkillValH)
local MaxSkillBaseStr = strformat("0x%X", MaxSkillBase)
local SkillAndStr = strformat("0x%X", SkillAnd)
local SkillAndXStr = strformat("0x%X", SkillAndX)

local MasterBit, GrandMasterBit = ExpertBit + 1, ExpertBit + 2
local ExpertBitH = ExpertBit - 8
local MasterBitH, GrandMasterBitH = ExpertBitH + 1, ExpertBitH + 2

local ExpertBitV = pow(2, ExpertBit)
local ExpertBitVH = ExpertBit > 7 and pow(2, ExpertBitH) or 0
local MasterBitV = ExpertBitV * 2
local MasterBitVH = MasterBit > 7 and pow(2, MasterBitH) or 0
local GrandMasterBitV, GrandMasterBitVH = ExpertBitV * 4, pow(2, GrandMasterBitH)
local ExpertBitStr = strformat("0x%X", ExpertBit)
local MasterBitStr = strformat("0x%X", MasterBit)
local GrandMasterBitStr = strformat("0x%X", GrandMasterBit)
local ExpertBitVStr, ExpertBitVHStr = strformat("0x%X", ExpertBitV), strformat("0x%X", ExpertBitVH)
local MasterBitVStr, MasterBitVHStr = strformat("0x%X", MasterBitV), strformat("0x%X", MasterBitVH)
local GrandMasterBitVStr = strformat("0x%X", GrandMasterBitV)
local GrandMasterBitVHStr = strformat("0x%X", GrandMasterBitVH)

-- Modifying original SplitSkill to work without % division, since value with bonus should be able to go above 0x40.
--[[
SplitSkill = function(val)
	local n, mast
	if val >= 0x100 then
		mast = 4
		n = val - 0x100
	elseif val >= 0x80 then
		mast = 3
		n = val - 0x80
	elseif val >= 0x40 then
		mast = 2
		n = val - 0x40
	elseif val >= 1 then
		mast = 1
		n = val
	else
		n = 0
		mast = 0
	end
	return n, mast
end
]]
function SplitSkill(val)
	local n = val % 0x100
	local mast
	if val >= 0x1000 then
		mast = 4
	elseif val >= 0x800 then
		mast = 3
	elseif val >= 0x400 then
		mast = 2
	elseif val >= 1 then
	--elseif val >= 0x200 then
		mast = 1
	else
		mast = 0
	end
	return n, mast
end

local ConvertMastery = {[0] = 0, [1] = 0, [2] = 0x400, [3] = 0x800, [4] = 0x1000}

JoinSkill = function(skill, mastery)
	if skill > MaxSkillVal then
		Log(Merge.Log.Error, "Incorrect skill value %d at JoinSkill()", skill)
		skill = MaxSkillVal
	end
	if mastery > 4 then
		Log(Merge.Log.Error, "Incorrect mastery %d at JoinSkill()", mastery)
		mastery = 4
	end
	return skill + (ConvertMastery[mastery] or 0)
end

-- Remove general bonus limit

-- GetSkillMastery
mem.hook(0x455B09, function(d)
	_, d.eax = SplitSkill(d.ecx)
	if d.eax == 0 then
		d.eax = 1
	end
end)
mem.nop2(0x455B0E, 0x455B24)

-- GetSkill
mem.asmpatch(0x48F060, [[
and ecx, ]] .. MaxSkillValStr .. [[;
add ecx, ebx
cmp ecx, ]] .. MaxSkillValStr)
mem.nop(0x48F065, 3)
mem.asmpatch(0x48F06A, [[
and eax, ]] .. SkillAndStr .. [[;
add eax, ]].. MaxSkillValStr)
mem.nop(0x48F06F, 1)
--mem.asmpatch(0x48f065, "jmp absolute 0x48f072")
mem.asmpatch(0x48F076, [[
and eax, ]] .. SkillAndStr .. [[;
inc eax
jmp absolute 0x48F07E]])
mem.nop(0x48F07B, 1)

-- Skill upgrade
mem.asmpatch(0x4B0AA5, "lea ecx, [eax+ecx*2+379h]")
mem.asmpatch(0x4B0AAC, "and byte ptr [ecx], 0")
mem.asmpatch(0x4B0ACE, "lea ecx, [eax+ecx*2+379h]")
mem.asmpatch(0x4B0AD5, "or byte ptr [ecx], 10h")
mem.asmpatch(0x4B0B15, "lea ecx, [eax+ecx*2+379h]")
mem.asmpatch(0x4B0B1C, "or byte ptr [ecx], 8")
mem.asmpatch(0x4B0B52, "lea ecx, [eax+ecx*2+379h]")
mem.asmpatch(0x4B0B59, "or byte ptr [ecx], 4")

-- Character Skills panel
mem.asmpatch(0x418D8A, [[
mov [ebp-24h], edx
and dword ptr [ebp-24h], ]] .. MaxSkillValStr)

mem.asmpatch(0x418DF8, "test byte ptr [ebp-0Fh], 10h")
mem.asmpatch(0x418E22, "test byte ptr [ebp-0Fh], 8")
mem.asmpatch(0x418E30, "test byte ptr [ebp-0Fh], 4")

mem.asmpatch(0x41912D, [[
mov [ebp-24h], edx
and dword ptr [ebp-24h], ]] .. MaxSkillValStr)

mem.asmpatch(0x4191A5, "test byte ptr [ebp-0Fh], 10h")
mem.asmpatch(0x4191CF, "test byte ptr [ebp-0Fh], 8")
mem.asmpatch(0x4191DD, "test byte ptr [ebp-0Fh], 4")

mem.asmpatch(0x4194D7, [[
mov [ebp-24h], edx
and dword ptr [ebp-24h], ]] .. MaxSkillValStr)

mem.asmpatch(0x419545, "test byte ptr [ebp-0Fh], 10h")
mem.asmpatch(0x41956F, "test byte ptr [ebp-0Fh], 4")
mem.asmpatch(0x41957D, "test byte ptr [ebp-0Fh], 8")

mem.asmpatch(0x41987A, [[
mov [ebp-24h], edx
and dword ptr [ebp-24h], ]] .. MaxSkillValStr)

mem.asmpatch(0x4198E8, "test byte ptr [ebp-0Fh], 10h")
mem.asmpatch(0x419912, "test byte ptr [ebp-0Fh], 4")
mem.asmpatch(0x419920, "test byte ptr [ebp-0Fh], 8")

mem.asmpatch(0x419E59, [[
test word ptr [edx+ecx*2+0x378], ]] .. MaxSkillValStr)
mem.asmpatch(0x419F25, [[
test word ptr [edx+ecx*2+0x378], ]] .. MaxSkillValStr)

-- Increase skill
-- Required skillpoints
mem.asmpatch(0x43234B, [[
mov edx, eax
and edx, ]] .. MaxSkillValStr)
-- Increase skill
mem.asmpatch(0x432359, [[
mov dx, ax
and dx, ]] .. MaxSkillValStr .. [[;
cmp dx, ]] .. MaxSkillBaseStr)
mem.nop(0x43235E, 3)
mem.asmpatch(0x432374, [[
and eax, ]] .. MaxSkillValStr .. [[;
sub [ecx+0x1BF4], eax
]])
--
mem.asmpatch(0x4C9E71, [[
jnz absolute 0x4C9EDE
and eax, ]] .. MaxSkillValStr)
mem.asmpatch(0x4C9EB9, [[
and eax, ]] .. MaxSkillValStr .. [[;
push eax
push 0x4F2F78
]])
mem.asmpatch(0x4C9EDE, [[
and eax, ]] .. MaxSkillValStr .. [[;
inc eax
cmp [edi+0x1BF4], eax
]])

-- Base functions

--local getbonus = mem.asmproc(
--[[
; ecx - base, eax - with bonus
test ch, ch
jnz @gm

cmp ecx, 0x80
jg @mm

cmp ecx, 0x40
jg @em

jmp @end

@gm:
sub eax, 0x100
jmp @end

@mm:
sub eax, 0x80
jmp @end

@em:
sub eax, 0x40

@end:
retn
]]
--)

-- when ecx contain player ptr
--getbonus3 = mem.asmproc(
--[[
; eax - value with bonus
; ecx - player ptr
; esi - skill id

; 1. get raw skill

movzx ecx, word [ds:esi*2+ecx+0x378]

; 2. get bonus value

mov esi, eax
sub esi, ecx

; 3. split raw skill

and ecx, 0x3f

; 4. get bonus

add ecx, esi

retn
]]
--)

-- MonsterCastSpell
asmpatch(0x404D8F, [[
mov ebx, ecx
and ebx, ]] .. MaxSkillValStr)
--   Poison Spray shots
asmpatch(0x404E60, [[
mov ecx, eax
shl ecx, 1
dec ecx
jmp absolute 0x404E7F
]], 10)
--   Sparks count
asmpatch(0x4050A9, [[
mov ecx, eax
shl ecx, 1
inc ecx
jmp absolute 0x4050C9
]], 10)
--   Shrapmetal fragments
asmpatch(0x405203, [[
mov ecx, eax
shl ecx, 1
inc ecx
jmp absolute 0x405219
]], 10)
--   Meteor Shower meteors count
asmpatch(0x40563A, [[
mov dword ptr [ebp-0x9C], ecx
mov ecx, dword ptr [0xB21558]
mov dword ptr [ebp-0x84], eax
mov dword ptr [ebp-0x8C], ecx
mov ecx, dword ptr [ebp+0x10]
call absolute 0x455B09
inc eax
shl eax, 2
jmp absolute 0x405672
]])

-- Alchemy
mem.asmpatch(0x4157B4, [[
mov edi, eax
and edi, ]] .. MaxSkillValStr)
--mem.asmpatch(0x4157b4, [[
--push esi
--mov esi, 0x25
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov edi, ecx
--cmp eax, 0x13c
--jle @end
--mov eax, 0x13c
--@end:
--]])

-- Notifications
mem.asmpatch(0x417295, [[
mov cx, word [ds:ebx*2+esi+0x378]
and eax, ]] .. MaxSkillValStr .. [[;
push 0x4F3BB8
and ecx, ]] .. MaxSkillValStr)
mem.nop2(0x41729A, 0x4172A7)
--jmp absolute 0x41729a]])
--mem.asmpatch(0x41729a, "call absolute " .. getbonus)

mem.asmpatch(0x41741e, [[
mov cx, word [ds:edi]
and eax, ]] .. MaxSkillValStr .. [[;
and ecx, ]] .. MaxSkillValStr)
mem.nop2(0x417423, 0x417426)
--call absolute ]] .. getbonus .. [[;
--]])

mem.asmpatch(0x417463, [[
mov cx, word [ds:edi]
and eax, ]] .. MaxSkillValStr .. [[;
and ecx, ]] .. MaxSkillValStr)
mem.nop2(0x417468, 0x41746B)
--call absolute ]] .. getbonus .. [[;
--]])

-- Id Monster
mem.asmpatch(0x41E07C, [[
mov edi, ecx
and edi, ]] .. MaxSkillValStr)
--mem.asmpatch(0x41e07a, [[
--push esi
--mov esi, 0x22
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov edi, ecx
--cmp eax, 0x13c
--jle @end
--mov eax, 0x13c
--@end:
--mov ecx, eax
--]])

--
mem.asmpatch(0x4203C1, [[
and ecx, ]] .. MaxSkillValStr .. [[;
inc ecx
cmp eax, ecx
]])

-- Bless target type selection
asmpatch(0x425C00, [[
mov ecx, [ebp+8]
test ecx, ecx
jnz short 0x425C11 - 0x425C00
movzx ecx, word ptr [eax+398h]
]], 0x11)
asmpatch(0x425C11, [[
call absolute 0x455B09
cmp eax, 2
jmp short 0x425C59 - 0x425C11
]], 0xA)

-- Magic
mem.asmpatch(0x42621F, [[
mov edi, eax
and edi, ]] .. MaxSkillValStr)
--mem.asmpatch(0x42621f, [[
--push esi
--mov esi, dword [ss:ebp-0x20]
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov edi, ecx
--cmp eax, 0x13c
--jle @end
--mov eax, 0x13c
--@end:
--]])
mem.asmpatch(0x42623A, [[
mov edi, eax
and edi, ]] .. MaxSkillValStr)
-- Maybe use GetSkillMastery call instead of following patches
mem.asmpatch(0x426242, "test ah, 0x10")
mem.asmpatch(0x426250, [[
test ah, 8
jz absolute 0x42625D
mov dword ptr [ebp-0xC], 3
]])
mem.nop2(0x426255, 0x42625B)
mem.asmpatch(0x42625D, [[
test ah, 4
push 0
pop eax
]])

-- Quick Spell SP cost
mem.asmpatch(0x42E86B, [[
test ah, 0x10
jz absolute 0x42E87D
]])
mem.asmpatch(0x42E87D, [[
test ah, 0x8
jz absolute 0x42E88E
lea eax, [ecx+ecx*4]
]])
mem.asmpatch(0x42E88E, [[
test ah, 0x4
lea eax, [ecx+ecx*4]
]])

-- Spell scroll skill value
mem.asmpatch(0x4320D1, 'push ' .. JoinSkill(5, 3))

-- damageMonsterFromParty
--
mem.asmpatch(0x436FFB, [[
mov ax, [ebx]
and eax, ]] .. MaxSkillValStr)
-- MainHand Weapon
-- Mace Stun
mem.asmpatch(0x4371BE, [[
mov ebx, eax
and ebx, ]] .. MaxSkillValStr)
--mem.asmpatch(0x4371be, [[
--push esi
--mov esi, ebx
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov ebx, ecx
--cmp eax, 0x13c
--jle @end
--mov eax, 0x13c
--@end:]])

-- Mace Paralyze
mem.asmpatch(0x4371E9, [[
mov ebx, eax
and ebx, ]] .. MaxSkillValStr)
--mem.asmpatch(0x4371e9, [[
--push esi
--mov esi, 0x6
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov ebx, ecx
--cmp eax, 0x13c
--jle @end
--mov eax, 0x13c
--@end:]])

-- Staff Stun
mem.asmpatch(0x437215, [[
mov ebx, eax
and ebx, ]] .. MaxSkillValStr)
--mem.asmpatch(0x437215, [[
--push esi
--mov esi, 0x0
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov ebx, ecx
--cmp eax, 0x13c
--jle @end
--mov eax, 0x13c
--@end:]])

-- Mace Paralyze
mem.asmpatch(0x4377B8, [[
and ebx, ]] .. MaxSkillValStr .. [[;
imul ebx, 1E00h
]])
mem.nop(0x4377BD, 4)
--mem.nop(0x4377b8, 3)
--mem.asmpatch(0x4377af, [[
--push esi
--mov esi, 0x6
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov ebx, ecx
--cmp eax, 0x13c
--jle @end
--mov eax, 0x13c
--@end:
--mov ecx, eax
--call absolute 0x455b09
--]])

-- Unarmed
mem.asmpatch(0x4380C4, [[
and edi, ]] .. MaxSkillValStr .. [[;
cmp edx, edi
]])
--mem.nop(0x4380c4, 3)
--mem.asmpatch(0x4380a7, [[
--push esi
--mov esi, 0x21
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov edi, ecx
--cmp eax, 0x13c
--jle @end
--mov eax, 0x13c
--@end:
--mov ecx, eax
--call absolute 0x455b09
--]])

--
mem.asmpatch(0x439162, [[
and edx, ]] .. MaxSkillValStr .. [[;
mov ecx, esi
]])

-- evt.Cmd
-- evt.CheckSkill
mem.asmpatch(0x443C1B, [[
mov ebp, eax
shr ebp, ]] .. ExpertBitStr)
mem.asmpatch(0x443C26, [[
shr ebp, ]] .. MasterBitStr)
mem.asmpatch(0x443C32, [[
shr ebp, ]] .. GrandMasterBitStr .. [[;
and eax, ]] .. MaxSkillValStr)
mem.nop(0x443C38, 3)
mem.asmpatch(0x443CD6, [[
mov ecx, eax
shr ecx, ]] .. ExpertBitStr)
mem.asmpatch(0x443CE1, [[
shr ecx, ]] .. MasterBitStr)
mem.asmpatch(0x443CED, [[
shr ecx, ]] .. GrandMasterBitStr .. [[;
and eax, ]] .. MaxSkillValStr)
mem.nop(0x443CF3, 3)

-- evt.Cmp
mem.asmpatch(0x4476B8, [[
cmp ebx, ]] .. MaxSkillValStr .. [[;
movzx edi, word ptr [esi+eax*2+0x2F0]
]])
mem.asmpatch(0x4476CC, [[
and edi, ]] .. MaxSkillValStr .. [[;
jmp absolute 0x447AC8
]])

-- evt.Set
mem.asmpatch(0x4481B1, [[
cmp word ptr [ebp+0xC], ]] .. MaxSkillValStr .. [[;
jle absolute 0x4481D1
lea ecx, [edi+eax*2+0x2F0]
mov ax, [ecx]
and ax, ]] .. SkillAndXStr .. [[;
or ax, [ebp+0xC]
]])
mem.nop2(0x4481B7, 0x4481C9)
mem.asmpatch(0x4481D1, [[
mov dx, [ebp+0xC]
lea eax, [edi+eax*2+0x2F0]
xor ecx, ecx
mov cx, [eax]
and ecx, ]] .. MaxSkillValStr)
mem.nop2(0x4481D6, 0x4481E4)

-- evt.Add
mem.asmpatch(0x448AE2, [[
cmp word ptr [ebp+0xC], ]] .. MaxSkillValStr .. [[;
jle absolute 0x448B12
]])
mem.asmpatch(0x448AF8, [[
and eax, ]] .. MaxSkillValStr .. [[;
add eax, edx
cmp eax, ]] .. MaxSkillBaseStr .. [[;
jle absolute 0x448B05
push ]] .. MaxSkillBaseStr)
mem.nop2(0x448AFD, 0x448B04)
mem.asmpatch(0x448B05, [[
and ecx, ]] .. SkillAndStr .. [[;
or ecx, eax
]])
mem.asmpatch(0x448B12, [[
mov dx, [ebp+0xC]
]])
mem.asmpatch(0x448B1E, [[
mov cx, [eax]
and ecx, ]] .. MaxSkillValStr)

-- Monsters.txt
-- SpellSkill
mem.asmpatch(0x453598, [[
mov edi, [ebp+edi-0x1E0]
and eax, ]] .. MaxSkillValStr)
mem.nop(0x45359F, 3)
mem.asmpatch(0x4535B7, [[
jnz absolute 0x4535C2
or word ptr [esi+0x42], ]] .. ExpertBitVStr)
mem.asmpatch(0x4535D1, [[
jnz absolute 0x4535DC
or word ptr [esi+0x42], ]] .. MasterBitVStr)
local gm = "GM"
mem.asmpatch(0x4535EB, [[
jz short @gm
push ]] .. mem.topointer(gm) .. [[;
push edi
call absolute 0x4DA920
test eax, eax
pop ecx
pop ecx
jnz absolute 0x453BAD
@gm:
or word ptr [esi+0x42], ]] .. GrandMasterBitVStr)
mem.nop(0x4535F1, 4)
-- Spell2Skill
mem.asmpatch(0x4536B2, [[
mov edi, [ebp+edi-0x25C]
and eax, ]] .. MaxSkillValStr)
mem.nop(0x4536B9, 3)
mem.asmpatch(0x4536D1, [[
jnz absolute 0x4536DC
or word ptr [esi+0x44], ]] .. ExpertBitVStr)
mem.asmpatch(0x4536EB, [[
jnz absolute 0x4536F6
or word ptr [esi+0x44], ]] .. MasterBitVStr)
mem.asmpatch(0x453705, [[
jz short @gm
push ]] .. mem.topointer(gm) .. [[;
push edi
call absolute 0x4DA920
test eax, eax
pop ecx
pop ecx
jnz absolute 0x453BAD
@gm:
or word ptr [esi+0x44], ]] .. GrandMasterBitVStr)
mem.nop(0x45370B, 4)

-- Spell scroll skill value (Fire Aura etc.)
mem.asmpatch(0x466B88, 'push ' .. JoinSkill(5, 3))

-- Bow GM damage bonus
local BowDamageIncludeItemsBonus = Merge and Merge.Settings and Merge.Settings.Skills
		and Merge.Settings.Skills.BowDamageIncludeItemsBonus or 0

if BowDamageIncludeItemsBonus == 1 then
	-- Take skill bonus from items into account
	-- GetRangedDamageMin
	mem.asmpatch(0x48CA86, [[
	push 5
	mov ecx, esi
	call absolute 0x48EF4F
	and eax, ]] .. MaxSkillValStr)
	-- GetRangedDamageMax
	mem.asmpatch(0x48CAEE, [[
	push 5
	mov ecx, esi
	call absolute 0x48EF4F
	and eax, ]] .. MaxSkillValStr)
	-- CalcRangedDamage
	mem.asmpatch(0x48CBEB, [[
	push 5
	mov ecx, ebx
	sub ecx, 0x382
	call absolute 0x48EF4F
	and eax, ]] .. MaxSkillValStr)
else
	-- Use base skill value
	-- GetRangedDamageMin
	mem.asmpatch(0x48CA86, [[
	mov ax, [esi+0x382]
	and eax, ]] .. MaxSkillValStr)
	-- GetRangedDamageMax
	mem.asmpatch(0x48CAEE, [[
	mov ax, [esi+0x382]
	and eax, ]] .. MaxSkillValStr)
	-- CalcRangedDamage
	mem.asmpatch(0x48CBEB, [[
	mov ax, [ebx]
	and eax, ]] .. MaxSkillValStr)
end
mem.nop(0x48CA8B, 4)
mem.nop(0x48CAF3, 4)

-- GetMeleeDamageRangeText
-- DragonAbility
mem.asmpatch(0x48CC26, [[
and eax, ]] .. MaxSkillValStr .. [[;
lea edi, [eax+0xA]
]])
--mem.asmpatch(0x48cc26, [[
--push esi
--mov esi, 0x17
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov eax, ecx
--lea edi, [eax+0xa]
--]])

-- GetRangedDamageRangeText
-- DragonAbility
mem.asmpatch(0x48CCC3, [[
and eax, ]] .. MaxSkillValStr .. [[;
lea edi, [eax+0xA]
]])
--mem.asmpatch(0x48ccc3, [[
--push esi
--mov esi, 0x17
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov eax, ecx
--lea edi, [eax+0xa]
--]])

-- GetAttackDelay
-- Sword/Axe/Bow
mem.asmpatch(0x48D8CC, [[
mov ax, [ebx]
and eax, ]] .. MaxSkillValStr)
-- Armsmaster
mem.asmpatch(0x48D8FA, [[
mov edi, eax
and edi, ]] .. MaxSkillValStr)
--mem.asmpatch(0x48d8fa, [[
--push esi
--mov esi, 0x23
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov edi, ecx
--cmp eax, 0x13c
--jle @end
--mov eax, 0x13c
--@end:
--]])

-- GetResistance
-- Leather
mem.asmpatch(0x48DDB9, [[
mov ax, [esi+0x38A]
and eax, ]] .. MaxSkillValStr)
mem.nop(0x48DDBE, 4)

-- CalcStatBonusByItems
-- DragonAbility
mem.asmpatch(0x48E2FD, [[
mov ecx, edi
and ebx, ]] .. MaxSkillValStr)
-- Mind Magic
mem.asmpatch(0x48E75C, [[
mov ax, [edi+0x39A]
]])
-- Magic
mem.asmpatch(0x48E762, [[
and eax, ]] .. MaxSkillValStr .. [[;
shr eax, 1
]])
-- Body Magic
mem.asmpatch(0x48E9D2, [[
mov ax, [edi+0x39C]
]])
-- Air Magic
mem.asmpatch(0x48E9E8, [[
mov ax, [edi+0x392]
]])
-- Dark Magic
mem.asmpatch(0x48EA0F, [[
mov ax, [edi+0x3A0]
]])
-- Light Magic
mem.asmpatch(0x48EA34, [[
mov ax, [edi+0x39E]
]])
-- Fire Magic
mem.asmpatch(0x48EA4A, [[
mov ax, [edi+0x390]
]])
-- Earth Magic
mem.asmpatch(0x48EA60, [[
mov ax, [edi+0x396]
]])
-- Water Magic
mem.asmpatch(0x48EAC9, [[
mov ax, [edi+0x394]
]])
-- Spirit Magic
mem.asmpatch(0x48EADF, [[
mov ax, [edi+0x398]
]])

-- CalcStatBonusBySkills
-- Armsmaster
mem.asmpatch(0x48F0DD, [[
and edi, ]] .. MaxSkillValStr .. [[;
imul edi, [ebp-0x4]
]])
-- Blaster Shoot / Unarmed
mem.asmpatch(0x48F1B6, [[
mov eax, edi
and eax, ]] .. MaxSkillValStr)
-- Bow Shoot
mem.asmpatch(0x48F1C3, [[
mov eax, [ebp+0x8]
and eax, ]] .. MaxSkillValStr)
--
--mem.asmpatch(0x48f1e2, [[
--push esi
--mov esi, 0x21
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov edi, ecx
--test edi, edi
--je absolute 0x48f50f]])

--mem.asmpatch(0x48f1ec, [[
--mov ecx, eax
--xor esi, esi
--call absolute 0x455b09]])
--mem.nop(0x48f1b8, 3)

-- Staff/Dagger/Axe/Spear/Mace Damage
mem.asmpatch(0x48F28E, [[
mov eax, esi
and eax, ]] .. MaxSkillValStr)
--mem.asmpatch(0x48f259, [[
--push esi
--mov esi, edi
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov esi, ecx
--cmp eax, 0x13c
--jle @end
--mov eax, 0x13c
--@end:
--mov ecx, eax
--call absolute 0x455b09]])
--mem.nop(0x48f290, 3)
--mem.nop(0x48f27f, 2)

-- Unarmed Attack
mem.asmpatch(0x48F2CC, [[
mov eax, esi
and eax, ]] .. MaxSkillValStr)
--mem.asmpatch(0x48f2b3, [[
--push esi
--mov esi, 0x21
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov esi, ecx
--test esi, esi
--je absolute 0x48f50f]])
--mem.asmpatch(0x48f2bd, [[
--mov ecx, eax]])
--mem.nop(0x48f2ce, 3)

-- Blaster Attack
mem.asmpatch(0x48F384, [[
mov eax, esi
and eax, ]] .. MaxSkillValStr)
-- Staff Unarmed Attack
mem.asmpatch(0x48F3C4, [[
and esi, ]] .. MaxSkillValStr .. [[;
imul esi, ebx
]])
--mem.asmpatch(0x48f3af, [[
--push esi
--mov esi, 0x21
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov esi, ecx
--test esi, esi
--je absolute 0x48f3cd]])

--mem.asmpatch(0x48f3b5, [[
--cmp eax, 0x13c
--jle @end
--mov eax, 0x13c
--@end:
--mov ecx, eax
--call absolute 0x455b09]])
--mem.nop(0x48f3c4, 3)

-- Staff/Dagger/Axe/Spear/Mace Attack
mem.asmpatch(0x48F3CD, [[
mov     eax, [ebp+0x8]
and     eax, ]] .. MaxSkillValStr)
-- Armor Class
mem.asmpatch(0x48F4A4, [[
and edi, ]] .. MaxSkillValStr .. [[;
xor ecx, ecx
]])
--mem.asmpatch(0x48f49b, [[
--push esi
--mov esi, edx
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov edi, ecx
--cmp eax, 0x13c
--jle @end
--mov eax, 0x13c
--@end:
--mov ecx, eax
--call absolute 0x455b09]])
--mem.nop(0x48f4a4, 3)

-- Dodging Armor Class
mem.asmpatch(0x48F4F8, [[
and esi, ]] .. MaxSkillValStr .. [[;
xor ecx, ecx
]])
--mem.asmpatch(0x48f4e0, [[
--push esi
--mov esi, 0x20
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov esi, ecx
--cmp eax, 0x13c
--jle @end
--mov eax, 0x13c
--@end:
--mov ecx, eax
--call absolute 0x455b09]])
--mem.nop(0x48f4f8, 3)

--
mem.asmpatch(0x49019C, [[
test ch, 0x10
jz absolute 0x4901A5
]])
mem.asmpatch(0x4901A5, [[
test ch, 0x8
jz absolute 0x4901AD
push 3
]])
mem.asmpatch(0x4901AF, [[
test ch, 0x4
pop eax
setnz al
]])

-- Bodybuilding
mem.asmpatch(0x4901C6, [[
and  ecx, ]] .. MaxSkillValStr .. [[;
imul eax, ecx
]])
--mem.asmpatch(0x4901bf, [[
--push esi
--mov esi, 0x1b
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--push ecx
--mov ecx, eax
--call absolute 0x49019c
--pop ecx
--jmp absolute 0x4901c9]])

-- Meditation
mem.asmpatch(0x4901DB, [[
and  ecx, ]] .. MaxSkillValStr .. [[;
imul eax, ecx
]])
--mem.asmpatch(0x4901d4, [[
--push esi
--mov esi, 0x1c
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--push ecx
--mov ecx, eax
--call absolute 0x49019c
--pop ecx
--jmp absolute 0x4901de]])

-- Id Item
mem.asmpatch(0x490208, [[
mov eax, ecx
and eax, ]] .. MaxSkillValStr)
--mem.asmpatch(0x4901ff, [[
--push esi
--mov esi, 0x18
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--push ecx
--mov ecx, eax
--call absolute 0x49019c
--pop ecx
--mov edi, eax
--mov eax, ecx
--jmp absolute 0x49020d]])

-- Repair
mem.asmpatch(0x490255, [[
mov eax, ecx
and eax, ]] .. MaxSkillValStr)
--mem.asmpatch(0x49024c, [[
--push esi
--mov esi, 0x1a
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--push ecx
--mov ecx, eax
--call absolute 0x49019c
--pop ecx
--mov edi, eax
--mov eax, ecx
--jmp absolute 0x49025a]])

-- Merchant
mem.asmpatch(0x4902A3, [[
mov ecx, eax
and esi, ]] .. MaxSkillValStr)
mem.asmpatch(0x4902D0, [[
and edi, ]] .. MaxSkillValStr .. [[;
imul eax, edi
]])
--mem.asmpatch(0x4902a1, [[
--push esi
--mov esi, 0x19
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov esi, ecx
--mov ecx, eax]])

-- Perception
mem.asmpatch(0x49030E, [[
and esi, ]] .. MaxSkillValStr .. [[;
imul eax, esi
and edi, ]] .. MaxSkillValStr)
--mem.asmpatch(0x4902f1, [[
--push esi
--mov esi, 0x1d
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov edi, ecx
--mov ecx, eax
--call absolute 0x455b09]])
mem.nop(0x490314, 3)

-- Disarm Traps
mem.asmpatch(0x490336, [[
and esi, ]] .. MaxSkillValStr .. [[;
and edi, ]] .. MaxSkillValStr)
--mem.asmpatch(0x490330, [[
--push esi
--mov esi, 0x1f
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov esi, ecx
--mov edi, ebp
--mov ecx, eax]])
--mem.nop(0x490336, 3)

-- Learning
mem.asmpatch(0x49038D, [[
and ecx, ]] .. MaxSkillValStr .. [[;
imul eax, ecx
and edx, ]] .. MaxSkillValStr)
--mem.nop(0x490378, 7)
--mem.asmpatch(0x49037f, [[
--push esi
--mov esi, 0x26
--call absolute ]] .. getbonus3 .. [[;
--pop esi

--mov edx, ecx
--test edx, edx
--pop esi]])
mem.nop(0x490393, 3)

-- roster.txt
mem.asmpatch(0x494C3B, [[
mov edi, ]] .. GrandMasterBitVStr)
mem.asmpatch(0x494C5B, [[
mov edi, ]] .. MasterBitVStr)
mem.asmpatch(0x494C79, [[
jnz absolute 0x494C7E
mov edi, ]] .. ExpertBitVStr)

--
mem.asmpatch(0x4B0AA5, [[
lea ecx, [eax+ecx*2+0x378]
and word ptr [ecx], ]] .. MaxSkillValStr)
mem.nop(0x4B0AAC, 4)

--
mem.asmpatch(0x4B0E79, [[
and edi, ]] .. MaxSkillValStr .. [[;
cmp eax, edx
]])

-- Donation spell skill value
mem.asmpatch(0x4B5B9E, "or edx, " .. MasterBitVStr)

-- Lloyd's Beacon interface
--
mem.asmpatch(0x4D1485, [[
test ah, 0x10
mov dword ptr [ebp-0x14], 1
jnz absolute 0x4D14A2
test ah, 0x8
jnz absolute 0x4D14A2
test ah, 0x4
]])
mem.nop2(0x4D148A, 0x4D1497)

--
mem.asmpatch(0x4D173D, [[
test ah, 0x10
mov dword ptr [ebp-0x18], 1
jnz absolute 0x4D1756
test ah, 0x8
jnz absolute 0x4D1756
test ah, 0x4
]])
mem.nop2(0x4D1747, 0x4D174F)

Log(Merge.Log.Info, "Init finished: %s", LogId)
