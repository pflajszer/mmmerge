Log(Merge.Log.Info, "Init started: MergeFunctions.lua")

-- Create null-terminated string from given lua string
function mem_cstring(str)
	local ptr = mem.malloc(#str + 1)
	mem.copy(ptr, str.."\0", #str + 1)
	return ptr
end

-- Filter table records that satisfy all the conditions.
--   Conditions are given in triplets: field name, comparison operator, value.
local function table_filter(t, preserve_indexes, ...)
	local function cmp(op, value1, value2)
		if op == "=" then return value1 == value2 end
		if op == "!=" then return value1 ~= value2 end
		if op == ">" then return value1 > value2 end
		if op == ">=" then return value1 >= value2 end
		if op == "<" then return value1 < value2 end
		if op == "<=" then return value1 <= value2 end
		return false
	end

	if t == nil then return nil end
	local args = { ... }
	local n = #args
	if math.floor(n / 3) * 3 ~= n then
		Log(Merge.Log.Warning, "table.filter: invalid number of arguments")
		return nil
	end
	local res = {}
	for k, v in pairs(t) do
		local add = true
		for i = 1, n / 3 do
			if not cmp(args[i * 3 - 1], v[args[i * 3 - 2]], args[i * 3]) then
				add = false
			end
		end
		if add then
			if preserve_indexes == 1 or preserve_indexes == true then
				table.insert(res, k, v)
			else
				table.insert(res, v)
			end
		end
	end
	return res
end

table.filter = table_filter

-- Check if table is empty.
local function table_isempty(t)
	return next(t) == nil
end

table.isempty = table_isempty

function CheckClassInParty(class)
	local result = false
	for _, pl in Party do
		if pl.Class == class then
			result = true
			break
		end
	end
	return result
end

function RosterHasAward(award)
	for idx = 0, Party.PlayersArray.count - 1 do
		local player = Party.PlayersArray[idx]
		if player.Awards[award] then
			return true
		end
	end
	return false
end

Log(Merge.Log.Info, "Init finished: MergeFunctions.lua")
